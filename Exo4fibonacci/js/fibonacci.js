//Affiche les 17 premiers termes de la suite de Fibonacci
//Auteur: Corentin Thelie
alert(affFibonacci(17));
function affFibonacci(prmRep) {
    let nb1 = 0;
    let nb2 = 1;
    let affichage = "0-1-";
    for (let i = 2; i <= prmRep; i++) {
        let somme = nb1 + nb2;
        affichage = affichage + somme + "-";
        nb1 = nb2;
        nb2 = somme;

    }
    return affichage;
}