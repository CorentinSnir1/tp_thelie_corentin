//Afficher la conversion de Euro(s) vers Dollar(s)
//Auteur : Corentin Thelie

let valMax = prompt("Valeur max en euros : ");
affConvEuroDollar(valMax);

function affConvEuroDollar(prmValMax) {
    //Afficher les conversions euros vers dollars jusqu'a prmValMax euros

    const TAUX = 1.65; //taux de change  1€ = 1.65$ 
    let valEuro = 1;
    let valDollar = valEuro * TAUX;
    let affichage = "";
    //Boucle d'affichage 
    while (valEuro <= prmValMax) {
        //Gestion de l'affichage d'une ligne x euros = x dollars
        affichage = affichage + valEuro + " euro(s) = " + valDollar.toFixed(2) + " dollar(s)" + "\n";
        valEuro = valEuro * 2
        valDollar = valEuro * TAUX;
    }
    //Affichage final des conversions
    alert(affichage);

}