//Afficher les 20 premiers multiples de 7
//Auteur : Corentin Thelie
function affMult(prmRep, prmTermes) {
    //Afficher les multiples de 7 pour prmRep

    let termes = 0;   //Variable des nombres de temres a utiliser 
    let affichage = "";    //Variable servant a l'affichage 
    //Boucle d'affichage
    for (let i = 0; i != prmRep; i++) {

        termes += prmTermes;
        affichage += " " + parseInt(termes);
    }
    //Affichage des 20 premiers termes
    alert("Les 20 premiers temres sont : " + affichage + "\n");
}
//Appel de fonction d'affichage
affMult(20, 7);